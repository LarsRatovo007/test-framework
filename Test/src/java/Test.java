/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

import anot.Uri;
import java.sql.Date;
import java.util.HashMap;
import utils.View;

/**
 *
 * @author lars
 */
public class Test {

    String nom;
    String prenom;
    int age;
    Date dateNaissance;

    
    @Uri("setname")
    public View getName() {
        View v = new View("hello.jsp", null);
        return v;
    }

    @Uri("show")
    public View show() {
        HashMap<String, Object> data = new HashMap<>();
        data.put("nom", getNom());
        data.put("prenom", getPrenom());
        data.put("age", getAge());
        data.put("dateNaissance", dateNaissance);
        View v=new View("show.jsp", data);
        return v;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }
}
