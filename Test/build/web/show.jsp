<%-- 
    Document   : hello.jsp
    Created on : 9 nov. 2022, 22:33:01
    Author     : lars
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.Date" %>
<%
    String nom=(String)request.getAttribute("nom");
    String prenom=(String)request.getAttribute("prenom");
    int age=(Integer)request.getAttribute("age");
    Date dateNaissance=(Date)request.getAttribute("dateNaissance");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>My Id</title>
    </head>
    <body>
        <h1>Nom : <% out.print(nom); %></h1>
        <h2>prenom : <% out.print(prenom); %></h2>
        <h2>Age : <% out.print(age); %></h2>
        <h2>Date de naissance : <% out.print(dateNaissance.toString()); %></h2>

    </body>
</html>
